from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse

class PublishedManager(models.Model):
	def get_queryset(self):
		return super(PublishedManager, self).get_queryset().filter(status='published')

class Post(models.Model):
	STATUS_CHOICES = (
		('draft', 'working'),
		('published', 'published'),
	)
	title = models.CharField(max_length=200)
	slug = models.SlugField(max_length=200, unique_for_date='publish')
	author = models.ForeignKey(User, related_name='posts')
	body = models.TextField()
	publish = models.DateTimeField(default=timezone.now)
	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)
	status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='draft')
	objects = models.Manager()
	published = PublishedManager()

	class Meta:
		ordering = ('-publish',)

	def get_absolute_url(self):
		return reverse('testapp:post_detail',args=[self.publish.year, self.publish.strftime('%m'), self.publish.strftime('%d'),self.slug])

	def __str__(self):
		return self.title
